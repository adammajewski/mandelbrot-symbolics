


Code from the [mandelbrot-symbolics library](https://code.mathr.co.uk/mandelbrot-symbolicsics) by [Claude Heiland-Allen](http://mathr.co.uk/)
# description	

symbolic algorithms related to the Mandelbrot set   

# c programs 

## m-binangle-from-rational

code : [m-binangle-from-rational.c](m-binangle-from-rational.c)  
  
description:
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/mandelbrot-symbolics)






# License

This project is licensed under the  Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details  

# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




## Git
```
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-symbolics.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
