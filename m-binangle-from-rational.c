#include <stdio.h>
#include <stdlib.h>
//#include <mandelbrot-symbolics.h>
#include <stdbool.h>
#include <gmp.h>
//
#include <assert.h>
#include <pari/pari.h>

/*
description	symbolic algorithms related to the Mandelbrot set
owner	Claude Heiland-Allen
last change	Thu, 21 Dec 2017 22:38:10 +0200 (20:38 +0000)
URL	https://code.mathr.co.uk/mandelbrot-symbolics.git

Name: mandelbrot-symbolics
Description: Symbolic algorithms related to the Mandelbrot set
Version: 0.1.0.0
URL: https://code.mathr.co.uk/mandelbrot-symbolics
Libs: -L${libdir} -lmandelbrot-symbolics
Libs.private: -lpari -lmpc -lmpfr -lgmp -lm
Cflags: -I${includedir}



c/bin/m-binangle-from-rational.c


gcc m-binangle-from-rational.c  -lpari -lmpc -lmpfr -lgmp -lm -Wall
./a.out 3/7
.(011)




cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-symbolics.git
git add .
git commit -m "Initial commit"
git push -u origin master


*/


// from c/include/mandelbrot-symbolics.h
struct m_block {
  mpz_t bits;
  int length;
};
typedef struct m_block m_block;



struct m_binangle {
  m_block pre;
  m_block per;
};
typedef struct m_binangle m_binangle;


// from c/lib/m_block.c

 void m_block_init(m_block *b) {
  mpz_init(b->bits);
  b->length = 0;
}

 void m_block_clear(m_block *b) {
  mpz_clear(b->bits);
}

 void m_block_empty(m_block *b) {
  mpz_set_si(b->bits, 0);
  b->length = 0;
}

 void m_block_append(m_block *o, const m_block *l, const m_block *r) {
  if (o == r) {
    mpz_t rbits;
    mpz_init(rbits);
    mpz_set(rbits, r->bits);
    mpz_mul_2exp(o->bits, l->bits, r->length);
    mpz_ior(o->bits, o->bits, rbits);
    o->length = l->length + r->length;
    mpz_clear(rbits);
  } else {
    mpz_mul_2exp(o->bits, l->bits, r->length);
    mpz_ior(o->bits, o->bits, r->bits);
    o->length = l->length + r->length;
  }
}

 void m_block_concatmap(m_block *o, const m_block *i, const m_block *lo, const m_block *hi) {
  if (o == i || o == lo || o == hi) {
    m_block o2;
    m_block_init(&o2);
    m_block_empty(&o2);
    for (int k = 0; k < i->length; ++k) {
      m_block_append(&o2, &o2, mpz_tstbit(i->bits, i->length - 1 - k) ? hi : lo);
    }
    mpz_set(o->bits, o2.bits);
    o->length = o2.length;
    m_block_clear(&o2);
  } else {
    m_block_empty(o);
    for (int k = 0; k < i->length; ++k) {
      m_block_append(o, o, mpz_tstbit(i->bits, i->length - 1 - k) ? hi : lo);
    }
  }
}

 const char *m_block_from_string(m_block *b, const char *s) {
  mpz_set_si(b->bits, 0);
  int i;
  for (i = 0; s[i] == '0' || s[i] == '1'; ++i) {
    mpz_mul_2exp(b->bits, b->bits, 1);
    if (s[i] == '1') {
      mpz_setbit(b->bits, 0);
    }
  }
  b->length = i;
  return s + i;
}

 void m_block_to_string(char *s, const m_block *b) {
  for (int i = 0; i < b->length; ++i) {
    s[i] = '0' + mpz_tstbit(b->bits, b->length - 1 - i);
  }
  s[b->length] = 0;
}

// from c/lib/m_symbolics.c
static bool m_symbolics_initted = false;
static bool m_symbolics_atexit = false;

 void m_symbolics_exit(void) {
  if (m_symbolics_initted) {
    pari_close_opts(INIT_DFTm);// | INIT_noIMTm);
    m_symbolics_initted = false;
  }
}

 void m_symbolics_init(void) {
  if (! m_symbolics_initted) {
    pari_init_opts(500000, 0, INIT_DFTm);// | INIT_noIMTm);
    m_symbolics_initted = true;
    if (! m_symbolics_atexit) {
      atexit(m_symbolics_exit);
      m_symbolics_atexit = true;
    }
  }
}




/* {{{ pari-gnump {{{ */

/*
Copyright Â© 2014 Andreas Enge <andreas.enge@inria.fr>

This file is part of pari-gnump.

Pari-gnump is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

Pari-gnump is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pari-gnump.  If not, see <http://www.gnu.org/licenses/>.
*/


/****************************************************************************/
/*                                                                          */
/* Functions converting between pari and mpz                                */
/*                                                                          */
/****************************************************************************/

static void mpz_set_GEN (mpz_ptr z, GEN x)
   /* Sets z to x, which needs to be of type t_INT. */

{
   const long lx = lgefint (x) - 2;
   const long sign = signe (x);
   int i;

   assert (sizeof (long) == sizeof (mp_limb_t));

   if (typ (x) != t_INT) {
#if 0
      pari_err_TYPE ("mpz_set_GEN", x);
#endif
   } else {
   if (sign == 0)
      mpz_set_ui (z, 0);
   else {
      mpz_realloc2 (z, lx * BITS_IN_LONG);
      z->_mp_size = sign * lx;
      for (i = 0; i < lx; i++)
         (z->_mp_d) [i] = *int_W (x, i);
   }
  }
}

/****************************************************************************/

static GEN mpz_get_GEN (mpz_srcptr z)
   /* Returns the GEN of type t_INT corresponding to z. */

{
   const long lz = z->_mp_size;
   const long lx = labs (lz);
   const long lx2 = lx + 2;
   int i;
   GEN x = cgeti (lx2);

   assert (sizeof (long) == sizeof (mp_limb_t));

   x [1] = evalsigne ((lz > 0 ? 1 : (lz < 0 ? -1 : 0))) | evallgefint (lx2);
   for (i = 0; i < lx; i++)
      *int_W (x, i) = (z->_mp_d) [i];

   return x;
}

/* }}} pari-gnump }}} */






// from c/lib/m_binangle.c
//static int m_period_pari(const mpz_t den);


static int m_period_pari(const mpz_t den) {
  m_symbolics_init();
  mpz_t n;
  mpz_init(n);
  pari_sp av = avma;
  mpz_set_GEN(n, order(gmodulsg(2, mpz_get_GEN(den))));
  avma = av;
  int p = 0;
  if (mpz_fits_sint_p(n)) {
    p = mpz_get_si(n);
  }
  mpz_clear(n);
  return p;
}



 void m_binangle_init(m_binangle *a) {
  m_block_init(&a->pre);
  m_block_init(&a->per);
  a->per.length = 1;
}

 void m_binangle_clear(m_binangle *a) {
  m_block_clear(&a->pre);
  m_block_clear(&a->per);
}

 void m_binangle_from_rational(m_binangle *a, const mpq_t q) {
  mpq_t p;
  mpq_init(p);
  a->pre.length = mpz_scan1(mpq_denref(q), 0);
  mpz_fdiv_q_2exp(mpq_denref(p), mpq_denref(q), a->pre.length);
  mpz_fdiv_qr(a->pre.bits, mpq_numref(p), mpq_numref(q), mpq_denref(p));
  a->per.length = m_period_pari(mpq_denref(p));
  mpz_mul_2exp(a->per.bits, mpq_numref(p), a->per.length);
  mpz_sub(a->per.bits, a->per.bits, mpq_numref(p));
  mpz_fdiv_q(a->per.bits, a->per.bits, mpq_denref(p));
  mpq_clear(p);
}

 void m_binangle_to_rational(mpq_t q, const m_binangle *a) {
  mpz_mul_2exp(mpq_numref(q), a->pre.bits, a->per.length);
  mpz_sub(mpq_numref(q), mpq_numref(q), a->pre.bits);
  mpz_add(mpq_numref(q), mpq_numref(q), a->per.bits);
  mpz_set_si(mpq_denref(q), 0);
  mpz_setbit(mpq_denref(q), a->per.length);
  mpz_sub_ui(mpq_denref(q), mpq_denref(q), 1);
  mpz_mul_2exp(mpq_denref(q), mpq_denref(q), a->pre.length);
  mpq_canonicalize(q);
}

void m_binangle_canonicalize(m_binangle *a)
{
  mpq_t q;
  mpq_init(q);
  m_binangle_to_rational(q, a);
  m_binangle_from_rational(a, q);
  mpq_clear(q);
}

 int m_binangle_strlen(const m_binangle *a) {
  return 4 + a->pre.length + a->per.length;
}

 void m_binangle_to_string(char *s, const m_binangle *a) {
  int k = 0;
  s[k++] = '.';
  m_block_to_string(s + k, &a->pre);
  k += a->pre.length;
  s[k++] = '(';
  m_block_to_string(s + k, &a->per);
  k += a->per.length;
  s[k++] = ')';
  s[k] = 0;
}

 const char *m_binangle_from_string(m_binangle *a, const char *s) {
  const char *t = s;
  if (*t != '.') return 0;
  t = m_block_from_string(&a->pre, t + 1);
  if (*t != '(') return 0;
  t = m_block_from_string(&a->per, t + 1);
  if (*t != ')') return 0;
  if (a->per.length <= 0) return 0;
  return t + 1;
}

 void m_binangle_tune(m_binangle *o, const m_binangle *i, const m_block *lo, const m_block *hi) {
  m_block_concatmap(&o->pre, &i->pre, lo, hi);
  m_block_concatmap(&o->per, &i->per, lo, hi);
}






int main(int argc, char **argv) {
  if (! (argc > 1)) {
    printf(" ther is no argument. Argument is ratio of 2 decimal integers. Example : 3/4 ");
    return 1;
  }
  mpq_t q;
  mpq_init(q);
  mpq_set_str(q, argv[1], 10);
  mpq_canonicalize(q);
  m_binangle ba;
  m_binangle_init(&ba);
  m_binangle_from_rational(&ba, q);
  char *s = malloc(m_binangle_strlen(&ba));
  m_binangle_to_string(s, &ba);
  printf("%s\n", s);
  free(s);
  m_binangle_clear(&ba);
  mpq_clear(q);
  return 0;
}
